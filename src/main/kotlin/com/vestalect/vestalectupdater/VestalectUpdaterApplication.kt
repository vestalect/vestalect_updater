package com.vestalect.vestalectupdater

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VestalectUpdaterApplication

fun main(args: Array<String>) {
    runApplication<VestalectUpdaterApplication>(*args)
}
