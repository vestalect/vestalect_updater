package com.vestalect.vestalectupdater.home

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class HomeController() {

    private val log = LoggerFactory.getLogger(HomeController::class.java)

    @GetMapping("/")
    fun root(): String {
        return "/home.html"
    }

    @GetMapping("/home")
    fun home(): String {
        return "/home.html"
    }

}