package com.vestalect.vestalectupdater.coreUpdater

import com.vestalect.vestalectupdater.utils.GitLabWikiDAO
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.client.RestTemplate

@Controller
class CoreUpdaterController(val template: RestTemplate, val coreUpdaterService: CoreUpdaterService) {

    private val log = LoggerFactory.getLogger(CoreUpdaterController::class.java)

    @Value("\${vestalect.core.project_id}")
    private val core_project_id: Int ?= null

    @GetMapping("/core")
    fun core(model: Model): String {
        log.info("We have $core_project_id as the project id")

        model["installedVersion"] = coreUpdaterService.getLocalVersion()

        val projectDetailsContent = template.getForObject("https://gitlab.com/api/v4/projects/$core_project_id/wikis/Metadata", GitLabWikiDAO::class.java)!!.content
        val regex = """CURRENT_VERSION=(.+)$""".toRegex()
        model["publishedVersion"] = regex.find(projectDetailsContent)!!.groups[1]!!.value

        //TODO:Change log

        return "/core.html"
    }

    @GetMapping("/core/update")
    @ResponseBody
    fun coreUpdate() {
        coreUpdaterService.downloadAndInstallLatest()
    }

}