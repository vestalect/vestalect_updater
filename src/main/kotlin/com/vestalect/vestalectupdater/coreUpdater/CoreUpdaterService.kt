package com.vestalect.vestalectupdater.coreUpdater

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.util.concurrent.TimeUnit

@Service
class CoreUpdaterService(val template: RestTemplate) {

    private val log = LoggerFactory.getLogger(CoreUpdaterService::class.java)

    @Value("\${vestalect.core.project_id}")
    private val core_project_id: Int ?= null

    @Value("\${vestalect.core.artifact_path}")
    private val vestalect_artifact_path: String ?= null

    @Value("\${vestalect.core.download_filename}")
    private val vestalect_download_filename: String ?= null

    @Value("\${vestalect.core.unpack_dir}")
    private val vestalect_unpack_dir: String ?= null

    @Value("\${vestalect.core.unpack_cmd}")
    private val vestalect_unpack_cmd: String ?= null

    @Value("\${vestalect.core.install_cmd}")
    private val vestalect_install_cmd: String ?= null

    @Value("\${vestalect.core.metadata_path}")
    private val vestalect_metadata_path: String ?= null

    fun downloadAndInstallLatest() {
        downloadLatest()
        installLatest()
    }

    fun getLocalVersion(): String {
        val vestalectMetaData = File(vestalect_metadata_path).useLines { it.toList() }
        for (line: String in vestalectMetaData) {
            if (line.startsWith("CURRENT_VERSION")) {
                val regex = """^CURRENT_VERSION=(.+)$""".toRegex()
                return regex.find(line)!!.groups[1]!!.value
            }
        }

        return "ERROR"
    }

    private fun downloadLatest() {
        try {
            val vestalectLatestArchive = "$vestalect_artifact_path/$vestalect_download_filename"
            val url = URL("https://gitlab.com/vestalect/vestalect_core/-/jobs/artifacts/master/download?job=upload_artifacts")
            val connection = url.openConnection()
            connection.connect()

            // Download the file
            val inputStream = BufferedInputStream(url.openStream(), 8192)

            // Ensure the file is created
            File(vestalectLatestArchive).createNewFile()

            // Output stream
            val outputStream = FileOutputStream(vestalectLatestArchive)

            inputStream.use { input ->
                outputStream.use { output ->
                    input.copyTo(output)
                }
            }
        } catch (e: Exception) {
            throw e
//            log.error("There was an error downloading or writing the file to $vestalect_download_location: ", e.message)
        }
    }

    private fun installLatest() {
        // First unpack the downloaded tar file
        log.info("The unpack command is [$vestalect_unpack_cmd]")
        ProcessBuilder(*vestalect_unpack_cmd!!.split(" ").toTypedArray())
                .directory(File(vestalect_artifact_path))
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .start()
                .waitFor(60, TimeUnit.MINUTES)

        // Then run the install from within the extracted dir
        log.info("The install command is [$vestalect_install_cmd]")
        ProcessBuilder(*vestalect_install_cmd!!.split(" ").toTypedArray())
                .directory(File("$vestalect_artifact_path/$vestalect_unpack_dir"))
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .start()
    }
}