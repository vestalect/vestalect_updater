package com.vestalect.vestalectupdater.configuration

import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.client.RestTemplate
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver



@Configuration
internal class AppConfiguration {

    @Bean
    fun restTemplate(): RestTemplate {
        return RestTemplate()
    }

    @Bean
    fun objectMapperBuilder(): Jackson2ObjectMapperBuilder {
        val  builder = Jackson2ObjectMapperBuilder()
        builder.modulesToInstall(KotlinModule())
        return builder
    }

    @Bean
    fun thymeleafTemplateResolver(): ClassLoaderTemplateResolver {
        val thymeleafTemplateResolver = ClassLoaderTemplateResolver()
        thymeleafTemplateResolver.prefix = "templates/"
        return thymeleafTemplateResolver
    }
}