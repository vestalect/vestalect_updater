[![pipeline status](https://gitlab.com/vestalect/vestalect_updater/badges/master/pipeline.svg)](https://gitlab.com/vestalect/vestalect_updater/commits/master)
[![coverage report](https://gitlab.com/vestalect/vestalect_updater/badges/master/coverage.svg)](https://gitlab.com/vestalect/vestalect_updater/commits/master)

# vestalect_updater

Simple web app to download and install the latest artifacts from the vestalect_core project. This app will strictly be a dependency of vestalect_core and not a standalone installation.